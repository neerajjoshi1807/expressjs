# Basic Project Part 2 : (Task 2)

In this project, we will learn to map multiple Html files for a response to the client.

- First, let us make one more Html file inside express-application folder named : `aboutUs.html` and use `index.html` and `aboutUs.html` to map as response to client

- use `touch` command to create `aboutUs.html` and start coding inside it.

- copy the below content in aboutUs.html file

```html
<html>
  <head>
    <title>About Us Page</title>
  </head>
  <body>
    This is about us page. Write here something about the page.
  </body>
</html>
```

- Now let us edit our `app.js` to map AboutUs.html along with index.html

- open `app.js` and start coding

```js
// importing path module
const path = require("path");

// importing express module and assigning into express variable
const express = require("express");

// making an object of the express module. Now 'app' will act as object of the express.
const app = express();

// sending GET request to the server
// '/' denotes the default page and sending index.html as a response to default page using a callback function

app.get("/", (req, res) => {
  // defining path of our index.html file
  let pathToIndexHtml = path.join(__dirname, "index.html");

  // now use pathToIndexHtml ad send the file as response
  res.sendFile(pathToIndexHtml);
});

// Now, let's map our aboutUs.html as a response when the client request for /AboutUs on the server.

app.get("/aboutus", (req, res) => {
  // defining path of our aboutUs.html file
  let pathToAboutUsHtml = path.join(__dirname, "aboutUs.html");

  // now use pathToAboutUsHtml ad send the file as response
  res.sendFile(pathToAboutUsHtml);
});

// defining port number. you can take any port number
let port = 4000;

// make the server listen on this port and displaying some message on the terminal using a callback function

app.listen(port, () => {
  console.log("Application listening on port", port);
});

module.exports = app;
```

Now run the below command to check the mapping

```
node app.js
```

Now you will see the index.html at first, to check the AboutUs page mapping, add `/aboutus` in URL at the end.

Now next is to kill the process to make port reusable. So run the below commands

```
ps aux | grep node
kill <pid>
```
Now go to the root directory and test your task, using given command :

```shell
npm run test02
```

Hint:
- To go to the previous directory, use `cd ..`
