# Serving Static Files

We have learned how to create dynamic webpages using a template engine, but there are some content or features which need to be included in the views statically.

Express provides a built-in middleware `express.static` to serve static resources, such as images, CSS, JavaScript, etc.

You simply need to pass the name of the directory where you keep your static resources, to the `express.static` middleware function to start serving the resource directly.

For the sake of our simplicity, all images will be stored in a directory named `images`, all the CSS files will be saved in a directory named `css` etc. All these directories itself put inside another directory named `client`

Suppose we need to serve a static image in html file, we will put that image in the `images` directory inside `client` directory. Now, we have to make this **images** directory as a **static resource** to our server because, this **images** directory will contain a lot of static resources.

To do so we will put the following line of codes in our **server file**.
```
app.use(express.static(path.resolve(__dirname, "client/images")));
```

To show the image onto the browser, we need to put the following code into our ejs file.

Thus modify the `index.ejs` created in the previous article and add the following line of code inside the `<body>` tag.

```html
<img src="hello.png" />
```

Since we have made `images` directory as static resource to our server, the server will look for the image from the specified path only (`images` directory). We need not give the whole path again and again for each image.

* **`Note:`** If you want to implement it at your side, then you have to choose a picture of your choice & put it as `hello.png` in the "client/images" directory.

How could it be used in the code is shown in the upcoming articles?