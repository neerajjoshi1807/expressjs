const path=require("path");
let homePage=(req,res)=>
{
    let pathToHtml=path.join(__dirname,"../../client/views/homePage.html");
    res.sendFile(pathToHtml);
}
let aboutUs=(req,res)=>
{
    let pathToAbout=path.join(__dirname,"../../client/views/aboutUs.html");
    res.sendFile(pathToAbout);
}
let login=(req,res)=>
{
    let pathToLogin=path.join(__dirname,"../../client/views/login.html");
    res.sendFile(pathToLogin);
}
let signUp=(req,res)=>
{
    let pathToSign=path.join(__dirname,"../../client/views/signUp.html");
    res.sendFile(pathToSign);
}
module.exports = {
      homePage: homePage,
      aboutUs: aboutUs,
      login: login,
      signUp: signUp
    };
