# Middleware

Express provides various middleware function which is used to handle various types of requests.

Middleware is a function that have access to **request object** and **response objects**, along with a function `(callback)`, called **`next()`**. The `next()` function is a function in the Express router which, when called, executes the middleware succeeding the current middleware.

Middleware functions get executed between the `incoming request` and `response output`. We can have multiple middleware functions in between. The following image visualizes the execution of middleware functions.

`middleware A` below will execute before `middleware B`, `middleware B` before `middleware C`. We can pass a variable from one middleware to another. The `next()` in `middleware A` will tell the path to execute `middleware B` after `middleware A` has finished its execution.

![](https://cdn-images-1.medium.com/max/800/1*WTcjbYL3fmFw5XW6fq408g.png)

## Writing a Middleware Function

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/Workspace/middleware.png)

`app.use()` is a method which is used to invoke any middleware function which we need to use. Let us understand through an example:

```js
var express = require("express"); //Including the express module
var app = express(); // Creating an Express object

//Writing a middleware function
var requestTime = function(req, res, next) {
  req.requestTime = Date.now();
  next();
};
```

The above function `requestTime` is a function that will give the time and date when the request is made. We can use this middleware function using the following code:

```js
app.use(requestTime);
```

We will now map our middleware function with a path or url, which when called, this middleware function will be executed. Also, we must ensure what type of HTTP request will be made to execute the function. We will map it using the following line of code:

```js
app.get("/", function(req, res) {
  responseText = "Requested at: " + req.requestTime;
  res.send(responseText);
});

app.listen(3000);
```

- `app.get()` denotes that the middleware function will be applied `get` type of HTTP request.
- `/` is the path for which the `requestTime()` middleware function applies.

In later articles, we will use various built-in middleware functions like a router, body-parser, morgan, compression, cors, etc.