const path=require("path");
const express=require("express");
const app=express();
app.get("/",(req,res)=>
{
    let pathToIndexHtml=path.join(__dirname,"index.html");
    res.sendFile(pathToIndexHtml);
});
app.get("/aboutus",(req,res)=>
{
    let pathToaboutUsHtml=path.join(__dirname,"aboutUs.html");
    res.sendFile(pathToaboutUsHtml);
});

app.get("/login",(req,res)=>
{
    let pathToLoginHtml=path.join(__dirname,"login.html");
    res.sendFile(pathToLoginHtml);
});

app.get("/signup",(req,res)=>
{
    let pathToSignUpHtml=path.join(__dirname,"signUp.html");
    res.sendFile(pathToSignUpHtml);
});

let port = 4000;

app.listen(port, () => {
  console.log("Application listening on port", port);
});

module.exports = app;