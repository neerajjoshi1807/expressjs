# MVC

Let us consider a scenario first in which we have to create 100 Html files for a website.

Now we will have to serve 100 responses inside our `app.js` which will make it so large and unreadable. Large and unreadable files are not good when the project is done in a team, as the other person will not be able to read that file, so to avoid that we shift towards modularity in which we break that large code in separate independent functions or modules. Hence, MVC comes as a solution.

The Express-based application is built on the MVC pattern. Let us understand the MVC pattern.

The _Model-View-Controller (MVC)_ is a design pattern that splits an application into three main logical components: _model_, _view_ and _controller_. Each of these components is built to handle and develop separate aspects of an application.

This is done to disconnect internal representations of information (Database) from the way information is presented to and accepted by the user (User Interface). MVC is one of the most frequently used industry-standard web development framework to create scalable and extensible projects.

- **Model:** Model represents the structure of data, the format and the rules with which it is stored. It maintains the data of the application. Essentially, it is the database part of the application.

- **View:** View is what is presented to the user (UI). View works with the Model and presents data to the user, the way he wants. A user can also be allowed to make changes to the data presented to him. Views consist of static and dynamic pages that are sent to the user when the user requests them.

* **Controller:** The controller controls the requests of the user and then generates an appropriate response that is sent to the view. Typically, the user interacts with the View, which in turn generates the appropriate request, this request will be handled by a controller. The controller renders the appropriate view with the data as a response.

So, to sum it up:

- Model handles the data part.
- View handles User Interface part.
- Controller handles the request-response cycle.

![Mvc](https://i.ibb.co/0yRdJwJ/mvc.png)

Now let us apply MVC approach to our project in which we had 4 html pages. For this refer next article.
