# Template Engine and EJS File

MVC framework includes model--view--controllers. Views can be HTML files, static content, etc.

We sent static HTML pages as a response in previous articles. But if we want to send a dynamic page to the server, we can use the `script` tag. But there is another way to do this: using a template engine.

To create dynamic HTML pages(in creating views) we use `template engines`.
Let us learn about the Template engine and "ejs".

Javascript contains a template engine that enables us to add dynamic logic to static HTML pages. They provide us to write a javascript code and include variables in an HTML file. There are various template engines like _mustache, Nunjucks, Handlebars, ejs_, etc.

We will be using `EJS` as our template engine. Therefore, let us understand the working of a template engine using `ejs`.

EJS is a type of template engine which stands for "Embedded JavaScript".

It helps to embed variables, loops, etc to the HTML pages without the usage of `<script>` tag.

For example:

```html
<%= 5 + 5 %>
```

What does the above code do? It prints `10` in the browser. It calculated the above expression `5+5` and writes the output over the browser.
But if it was plain HTML like:

```html
<p>5+5</p>
```

It would have printed `5+5`, i.e. as a plain text in the browser.

This is what a template engine does. We can add whatever variable we are passing from the server-side to the client's side using EJS and it won’t be rendered as HTML. Rather, it takes the value from the javascript file. Let us take another example:

The HTML file:

```html
<h1>Example variable name: <%= exampleVar %></h1>
```

JS file:

```js
app.get("/", function(req, res) {
  res.render("index.ejs", { exampleVar: Javascript });
});
//the "exampleVar" being the name of our variable from the EJS file
//the "JavaScript" the value assigned to the variable
```

In the above example, we have used an `app.get()` the function which sends a _get request_ to the server.

`res.render` is a function that sends/renders the file to the server in HTML format. We will learn its usage in upcoming articles. But the main point to be considered is the variable `exampleVar` which must not be printed as plain text, rather its value must be fetched.

The above HTML code gives an output:

```
Example variable name: Javascript
```

Not only variables, but the javascript loops also work in template engine. For example:

```html
<% if( exampleVar === "JavaScript") { %>
<p>Good Choice</p>
<% } %>
```

The above will add only the contents of the paragraph to our HTML, if the condition is true.

Another example:

```html
<% for(var i = 1; i <= 10; i++) { %> <%= i %> <% } %>
```

In the above code we have used a for loop from 1 to 10. The expression `<% = i %>` indicates that the value of `i` must be printed in the browser. The value of `i` changes from 1 to 10. It produces the following output.

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/Workspace/loopEJS.png)

Besides significantly speeding up front-end development, JavaScript template engines also make debugging and maintenance simpler and faster.
