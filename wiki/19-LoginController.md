# Sending and Receiving the Signup data

We have created a table in MySQL which will contain the user data, but now we want to run a function that will put the actual sign-up form data into the database. The data of the sign-up form is sent a request to the server and the server responds to the client with the "profile.ejs" page.

We have created a separate file known as `LoginController.js` which will capture the form data from "signup.ejs" and send it as a request to the server.

In the following code, we have only included the `sqlite.js` file using the `require` method.

```js
const dbConn = require("../databases/sqlite.js"); //Line1
```

In the code below, we have executed the `sqlite.js` file which means we have created a connection with the database and also created a table named `user` if it did not exist.

```js
const User = dbConn.User; //Line2
```

The following code exports the signup function.

```js
module.exports = {
  signup: signup
};
```

We will now create the main function which will run a query to put all the data filled up by the user in the Signup form into the database and create a response to the client.

```js
function signup(req, res) {
  const { name, email, password } = req.body;         //Line3
  if (!(name && email && password))                   //Line4
    return res.render("signup", {                     //Line5
      msg: "Please enter all the required details"
    });
  else {
    User.create({           //Line6
      name,
      email,
      password
    })
      .then(user => {       
        if (user) {
          console.log(user);
          return res.render("profile", {        //Line7
            msg: "User successfully created",
            user: user.name
          });
        }
      })
      .catch(err => {
        return res.render("profile", { msg: "Error in creating user" });
      });
  }
}
```

Let us understand the code of the above function line by line.

- The data given by the user in the signup form will be stored in the `req.body` object. Thus we can extract those values. In _Line3_, "name", "email" and "password" are the variables to store the values contained in the request body.
- In _Line4_ and _Line5_, it is ensured that none of the fields(name, email, password) remains empty. If so, the server must send the same page as a response with a message to "enter all the required fields".
- If all the fields are filled by the user and the user submits it, then the data of the user sent as the request must be updated into the database. Therefore, in _Line6_, a query of `Sequelize` is executed. `User.create` creates a new user into the database.
- As discussed before, Sequelize works on the basis of _Promise_ which can be resolved or rejected. Therefore, in _Line7_, if the new user is created (i.e. promise gets fulfilled), then it must alert with a message "User successfully created". In _Line7_, also consider the second argument. It is an object which contains two keys-`msg` and `user`. These are the values that are passed dynamically to the variables, we created in `profile.ejs`.