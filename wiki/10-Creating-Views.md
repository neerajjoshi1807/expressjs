# Creating Views

We have just learned that Express projects are based on MVC design.

We will first create views. The view is something that the user sees. Thus creating views include the static Html pages or dynamic pages using `ejs` template engine.

Let's create a view named **`index.ejs`** in the views directory.

```html
 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home Page</title>
</head>
<body>
	<h1>HELLO WORLD!!!</h1>
</body>
</html>
```

The above code is a simple Html code that is self-explanatory.
The above file in saved in a directory named "views" which in turn is itself saved in another directory "client".