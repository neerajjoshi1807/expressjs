# Creating the view which will be displayed once signed up:


We need to create a view that will be sent as a response by the server once a user signs into the server.
The following line of code will be saved as `profile.ejs` in the `views` directory.
The code for the "profile.ejs" is as follows:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title></title>
  </head>
  <body>
    <h1>Profile</h1>

    <% if(user) {%> //Line1
    <hidden style="display:none;"><%= msg %></hidden> //Line2
    <h2>Hello <%= user %></h2>
    //Line3 <% } %>

    <script>
      var msg1 = document.getElementsByTagName("hidden");
      alert(msg1[0].innerHTML);
    </script>
  </body>
</html>
```

Consider the `if` block in HTML. This is made possible because this file is `ejs` file which takes in the value dynamically and renders the page as an HTML page.

- In the above code, at _Line1_, it is checked if the user is present or has been created in the database. If yes, then line 2 and 3 are executed.
- _Line2_ has a variable named `msg` which will get its value when this page will be rendered.
- Similarly in _Line3_, the value of the variable `user` will be the name of the _user_ who signs up on the server. This value will be given to the variable whenever requested.

The script written in the above code is used to display an alert message.`document.getElementsByTagName('hidden')` assigns the value to the variable `msg1` equal to the element whose tagname is `hidden`. Thus variable `msg1` takes its value from the `msg` in _Line2_. Once the variable `msg1` gets its value, that is, once a new user is created, it shows an alert in the browser.

Following is an example of the alert:

![]( https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/Workspace/alert.png)