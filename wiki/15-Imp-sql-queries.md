# Some important Sequelize queries which we will be using to access the database:

_Note that, that all the queries return a promise and thus `.then()` and `.catch()` functions are applicable._

- **Table_name.create()**  
  It is used to insert data into the table. For example, for the table `users` that we created above, the query can be executed as follows:

```js
User.create({
           'abc',
           'abc@gmail.com',
           'pass1'
       }).then((user)=>{
           console.log(user);
       }
```

This will create a new user in the user table whose name is "abc", email is "abc@gmail.com" and the password is "pass1".

- **Table_name.findOne()**  
  This query is used to find whether an entry exists in the table. For example:

```js
User.findOne({
      where : {
        email: 'abc@gmail.com'
      }
    }).then((user) => {
        console.log(user);
    }
```

The above code will fetch the first entry where `email = "abc@gmail.com"`.

- **Accessing a particular column from the whole table**

If you want to access all the values of a particular column, for example, if we need to access `email` of all the users in the table, we need to execute the following query:

```js
User.findAll({
          attributes: [['email','email_id']]
        }).then((user)=>{console.log(user);
        }
```

- `.findAll()` will find all the entries in the table. It takes an object as an argument which contains some predefined keys to which we can pass the values.
- In the above code, we have passed the value to the `attribute` key whose value is an array containing all the columns of the database table we want to access.
- Since we need only the emails contained in the table, thus we have passed the attribute name `email` in the array as the value of the `attribute` key.
- We can also use `alias` to access any attribute. In the above example, we have use `email_id` as an alias to `email`.