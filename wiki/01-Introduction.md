# Express js

The most common part of application development is handling different HTTP requests (e.g. `GET`, `POST`, `DELETE`, etc.), separately and handle requests at different URL paths ("_routes_"), serve static files, or use templates to dynamically create the response. All these tasks are done by writing the code yourself.

Express is a simple and flexible Node.js web application framework that provides a wide set of features for web and mobile applications.

We first need to install the _Express_ module by using the following command.

```
root@konfinity:usr/app # npm install express
```

Now, we look to the following code to illustrate how to use the _Express_ module.

![](https://www.guru99.com/images/NodeJS/010716_0613_NodejsExpre1.png)

Understanding the code line by line:

- In our first line of code, we are using the `require` function to include the "express module".

- Before we can start using the express module, we need to make an object of the express module. Thus in the second line, we have created that object. The `app` is the express object which provides various functions like `app.get()`, `app.set()`, `app.use()` etc which we will be using in proceeding articles.

- In the third line, `app.get()` is a method that sends the `GET` request to the server. This method takes two arguments: the first argument is the path on which the request is to be made. `/` denotes the default page.
  The second argument is the callback function which handles `request` and `response`. This function will be called whenever anybody browses to the root of our web application which is http://localhost:3000.
- In line 4 inside the callback function, we are sending the string "Hello World" back to the client. The 'res' parameter is used to send content back to the web page.

* In line 5, we request the server to listen to port no. 3000.

**The Express.js framework makes it very easy to develop an application which can be used to handle multiple types of requests like the GET, PUT, and POST and DELETE requests.**

To understand the above picture in better form, Let us do some small project setup and do some practicals. Refer to the next article i.e Basic Project setup.
