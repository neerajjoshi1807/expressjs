# The Final Express Setup:

Since now, you have learnt a lot of middlewares and its applicability. So we expect from you that you must implemented these middlewares in your server file.

So, let's have a final overview.

We have created all the required functions. Now we will modify our previously generated `app.js` file to call these functions and execute the middleware functions used. The code is as follows:

At first, we will import certain files we have already created and other libraries which we will be using later.

```js
const express = require("express");
const router = express.Router();
const app = express();
const cors = require("cors");
const compression = require("compression");
const bodyParser = require("body-parser");
const logger = require("morgan");
const path = require("path");
const mainRoutes = require("./backend/routes/MainRoutes");
```

In the following code, a middleware function called `cors()` is used. **Cors()** is `access-control-allow-origin`. A js application running in the browser can usually only access HTTP resources on the same domain (origin) that serves it but XHR and Fetch calls to another server will fail unless that server implements `cors`.

```js
app.use(cors()); //Line1
```

In the code below, we have used a middleware function `compression`. It decreases the size of the response body and hence increases the speed of the application. It compresses the file in `gzip` format which makes the execution faster.

```js
// for compressing data objects
app.use(compression()); //Line2
```

In the following code, another middleware function is used which is known as `body-parser`. Whenever a request is sent to the server using _Post method_, the request body contains the content. The `body-parser` middleware function helps to validate that content. As `req.body`(request body) shape is based on user-controlled input, all properties and values in this object are untrusted and should be validated before trusting.

Every request contains a number of headers, and `bodyParser.urlencoded()` is the middleware function which parses only those requests where the **Content-Type header** matches the **type** option in the request body. It accepts only _UTF-8_ encoding of the body and supports the extraction of _gzip_ and _deflate_ encodings. In other words, it is a middleware function that is required **to read the content of the body from where the request is made and will discard the extra things**.

```js
//Parse incoming request bodies in a middleware
app.use(bodyParser.urlencoded({ extended: true })); //Line3
```

In the code below, the `bodyParser.json()` is a middleware function that is used to parse the **JSON** object in the request body.

```js
// return middleware that only parses json
app.use(bodyParser.json()); //Line4
```

The rest of the code lines are same and they have been explained in previous articles.

```js
app.set("views", __dirname + "/client/views"); //line5

// ejs - for rendering ejs in html format
app.engine("html", require("ejs").renderFile); //Line6

// setting view-engine as ejs
app.set("view engine", "ejs"); //Line7

app.use(express.static(path.resolve(__dirname, "client/images"))); //Line8

// for logging purposes
app.use(logger("dev")); //Line9
```

In this we have used the router we created in `MainRoutes.js`

```js
// main route or default route
app.use("/", mainRoutes); //Line10
```

As explained in the previous articles, the following code is used to establish a server which will listen to port number 4000.

```js
app.set("port", process.env.PORT || 4000); //Line11
app.listen(app.get("port"), () => {
  //Line12
  console.log("Application running in port: " + app.get("port"));
});

module.exports = app;
```
Now go to the root directory and test your task, using given command :

```js
npm run test05
```

Hint:
- To go to the previous directory, use `cd ..`