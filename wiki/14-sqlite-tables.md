# Creating a table in the database

Now we will be requiring a table which will store the data from the sign-up form, we will create a table (named `user`) which has the following fields:

- name
- email
- password

Using Sequelize, we can create the table using `sequelize.define()`.

```js
const User = sequelize.define("user", {
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  }
});
```

In the above code, we have defined a table structure that contains three fields in which _email_ must be unique and none of the fields must remain empty.
In each field, the **`type`** denotes the data-type of the field which can be integer, or string, etc, the **`unique`** takes a boolean value which denotes that each user must enter a unique email, and the **`allowNull`** also takes boolean value which denotes whether a field is mandatory to be filled.

The above code just defines the structure of the table. The table is yet to be created. The table is created by running the following code:

```js
sequelize
  .sync()
  .then(() =>
    console.log(
      "users table has been successfully created, if one doesn't exist"
    )
  )
  .catch(error => console.log("This error occurred"));
```

`sequelize.sync()` creates all the tables which are defined using _sequelize.define()_. Since Sequelize is promise based, thus `.then()` and `.catch()` functions can be applied to perform certain tasks.

In the above code, after the table is created, the promise is either fulfilled and thus `.then()` the block gets executed which prints "users table has been successfully created if one doesn\'t exist", or the promise gets rejected and the `.catch()` the block gets executed which prints "This error occurred".

In the end, we will export the `User` table we have created.

```js
module.exports = {
  User: User
};
```