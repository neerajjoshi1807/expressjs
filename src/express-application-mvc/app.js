const express = require('express');
const routes = require('./backend/routes/htmlRoutes');
const path = require('path');

const app = express();

app.use('/', routes);


let port = 3000;
app.listen(port, () => {
	console.log('Application listening on port', port);
});

module.exports = app;

